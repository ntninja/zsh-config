# Import environment variables set in ~/.pam_environment after log in


# Enable “extended glob” feature (needed for trimming whitespace)
if [ -n "${BASH_VERSION:-}" ];
then
	# BASH ExtGlob; Source: https://unix.stackexchange.com/a/360648/47938
	function _pam_env_ltrim()
	{
		local extglob_state="$(shopt -p extglob)"
		shopt -s extglob
		
		echo -n "${1##+([[:blank:]])}"
		
		if [ "${extglob_state}" != "shopt -s extglob" ];
		then
			shopt -u extglob
		fi
	}
	function _pam_env_trim()
	{
		local extglob_state="$(shopt -p extglob)"
		shopt -s extglob
		
		local text="${1##+([[:blank:]])}"
		echo -n "${text%%+([[:blank:]])}"
		
		if [ "${extglob_state}" != "shopt -s extglob" ];
		then
			shopt -u extglob
		fi
	}
elif [ -n "${ZSH_VERSION:-}" ];
then
	# ZSH ExtGlob; Source: https://unix.stackexchange.com/a/362831/47938
	function _pam_env_ltrim()
	{
		emulate -L zsh       # default zsh behaviour locally
		setopt extendedglob  # with extendedglob for ## (= ERE +) below

		printf "%s" "${1##[[:blank:]]##}"
	}
	function _pam_env_trim()
	{
		emulate -L zsh       # default zsh behaviour locally
		setopt extendedglob  # with extendedglob for ## (= ERE +) below

		printf "%s" "${${1##[[:blank:]]##}%%[[:blank:]]##}"
	}
else
	echo "pam_environment: This script was only tested on BASH and ZSH!" >&1
	exit 254
fi


function _pam_env_log_error()
{
	printf "~/.pam_environment[%s]: %s\n" "${1}" "${2}" >&2
}


function _pam_env_expand_env()
{
	local name="${1}"
	local value="${2}"
	
	local escaping=
	local substing=0
	local subst_kind=
	local subst_var=
	
	printf "'"
	for (( idx=0; idx < ${#value}; idx++ ));
	do
		char="${value:${idx}:1}"
		
		if [[ ${substing} = 1 ]];
		then  # Expect opening curly brace after variable start
			if [[ ${char} = '{' ]];
			then
				substing=2
				subst_var=""
			else
				_pam_env_log_error "${name}" "Expandable variables must be wrapped in {}: <${char}> - ignoring"
				substing=0
				subst_kind=""
			fi
		elif [[ ${substing} = 2 ]];
		then  # Read variable name
			if [[ ${char} = "}" ]];
			then
				printf "'"
				
				if [[ ${subst_kind} = '$' ]];
				then
					printf '"${%s:-}"' "${subst_var}"
				elif [[ ${subst_kind} = '@' ]];
				then
					_escape "$(case "${subst_var}" in
						PAM_USER|PAM_RUSER)
							printf "%s" "$(id -un)"
						;;
						
						PAM_USER_PROMPT)
							printf "login: "
						;;
						
						PAM_TTY)
							printf "%s" "$(tty)"
						;;
						
						PAM_RHOST)
							printf "%s" "$(hostname)"
						;;
						
						HOME)
							printf "%s" "$(getent passwd $(id -u) | cut -d':' -f6)"
						;;
						
						SHELL)
							printf "%s" "$(getent passwd $(id -u) | cut -d':' -f7)"
						;;
					esac)"
				fi
				
				printf "'"
				
				substing=0
				subst_kind=""
				subst_var=""
			else
				subst_var+="${char}"
			fi
		elif [[ ${escaping} ]];
		then
			if [[ ${char} != '$' && ${char} = '@' ]];
			then
				_pam_env_log_error "${name}" "Unrecognized escaped character: <{char}> - ignoring"
			fi
			printf "%s" "${char}"
			escaping=
		elif [[ ${char} = '\' ]];
		then
			escaping=x
		elif [[ ${char} = '$' || ${char} = '@' ]];
		then
			substing=1
			subst_kind="${char}"
		elif [[ ${char} = "'" ]];
		then
			printf "'\"'\"'"
		else
			printf "%s" "${char}"
		fi
	done
	
	printf "'"
}


function _pam_env_parse_adv()
{
	local name="${1}"
	local cont="${2}"
	
	local -A options
	
	local p_curr_buf
	local p_opt_name
	local p_opt_value
	local p_is_quoted=''
	
	for (( idx=0; idx < ${#cont}; idx++ ));
	do
		char="${cont:${idx}:1}"
		
		if [[ -z ${p_opt_name} ]];  # Expect option name
		then
			if [[ ${char} = '=' ]];  # Start of value marker
			then
				p_opt_name="${p_curr_buf}"
				p_curr_buf=""
			elif [[ ${char} != '	' && ${char} != ' ' ]];  # Part of option name
			then
				p_curr_buf+="${char}"
			elif [[ -n ${p_curr_buf} ]];  # Option without value
			then
				options[${p_curr_buf}]=""
				p_curr_buf=""
			fi
		elif [[ -z ${p_opt_value} ]];  # Expect option value
		then
			if [[ -z ${p_curr_buf} ]] && [[ ${char} = '"' ]];  # Start of quoted string
			then
				p_is_quoted=x
			elif [[ ${char} = '	' || ${char} = ' ' ]] && ! [[ ${p_is_quoted} ]];  # End of value string
			then
				options[${p_opt_name}]="${p_curr_buf}"
				p_curr_buf=""
				p_opt_name=""
			elif [[ ${p_is_quoted} ]] && [[ ${char} = '"' ]];  # Possible end of quoted string
			then
				if [[ ${p_curr_buf:(-1)} = '\' ]];
				then  # Just an escapted quote
					p_curr_buf="${p_curr_buf:0:(-1)}\""
				else  # End of quoted value string
					p_opt_value="${p_curr_buf}"
					p_is_quoted=
					p_curr_buf=""
				fi
			else
				p_curr_buf+="${char}"
			fi
		elif [[ ${char} = '	' || ${char} = ' ' ]];  # Expect space after quoted value
		then
			options[${p_opt_name}]="${p_opt_value}"
			p_opt_name=
			p_opt_value=
		else  # Characters after final quote of quoted value
			_pam_env_log_error "${name}" "Quotes must cover the entire string: ${cont:${idx}}"
			return 1
		fi
	done
	
	if [[ -n ${p_opt_name} ]] && [[ -n ${p_opt_value} ]];
	then  # Quoted value at end of line
		options[${p_opt_name}]="${p_opt_value}"
	elif [[ -n ${p_opt_name} ]] && ! [[ ${p_is_quoted} ]];
	then  # Unquoted value at end of line
		options[${p_opt_name}]="${p_curr_buf}"
	elif [[ -z ${p_opt_name} ]] && [[ -n ${p_curr_buf} ]];
	then  # Final option name without value
		options[${p_curr_buf}]=""
	elif [[ -n ${p_opt_name} ]] && [[ ${p_is_quoted} ]];
	then  # Incomplete quoted value at end of line
		_pam_env_log_error "${name}" "Unterminated quoted string: ${cont:${idx}}"
		return 1
	fi

	# Override assignments are always processed before other assignments
	#
	# The man-page shows this in its examples where OVERRIDE is used to
	# assign a value from the PAM environment, while DEFAULT is used on the
	# same line to assign a default to the environment variable in case the
	# PAM variable expanded to nothing:
	#
	# > […]
	# > Set the REMOTEHOST variable for any hosts that are remote, default to "localhost" rather than not being set at all
	# >
	# >         REMOTEHOST     DEFAULT=localhost OVERRIDE=@{PAM_RHOST}
	# > […]
	
	if [[ -v options[OVERRIDE] ]];
	then
		local opt_raw_value="${options[OVERRIDE]}"
		
		# Special case: If we have an option such as “PATH OVERRIDE="@{HOME}/.local/bin:${PATH}"”,
		# generate code to that checks if $PATH still starts with the expansion of the appended
		# prefix/suffix and only does the prepend/append if prefix/suffix in the environment has
		# changed since pam_env.so has run
		local opt_raw_prefix="${opt_raw_value%%\$\{${name}\}*}"
		local opt_raw_suffix="${opt_raw_value##*\$\{${name}\}}"
		if [[ ${opt_raw_value} = "${opt_raw_prefix}\${${name}}${opt_raw_suffix}" ]];
		then  # If value matches "… \$\{<OPT_NAME>\} …"
			local opt_prefix="$(_pam_env_expand_env "${name}" "${opt_raw_prefix}")"
			local opt_suffix="$(_pam_env_expand_env "${name}" "${opt_raw_suffix}")"
			
			if [[ "${opt_prefix}" != "''" ]];
			then
				printf 'case "${%s:-}" in %s*) ;; *) export %s=%s"${%s:-}"; ;; esac\n' "${name}" "${opt_prefix}" "${name}" "${opt_prefix}" "${name}"
			fi
			if [[ "${opt_suffix}" != "''" ]];
			then
				printf 'case "${%s:-}" in *%s) ;; *) export %s="${%s:-}"%s; ;; esac\n' "${name}" "${opt_suffix}" "${name}" "${name}" "${opt_suffix}"
			fi
		else
			local opt_value="$(_pam_env_expand_env "${name}" "${opt_raw_value}")"
			printf 'export %s\n' "${name}=${opt_value}"
		fi
		
		unset 'options[OVERRIDE]'
	fi
	
	if [[ -v options[DEFAULT] ]];
	then
		local opt_value="$(_pam_env_expand_env "${name}" "${options[DEFAULT]}")"
		printf 'test -z "${%s:-}" && export %s\n' "${name}" "${name}=${opt_value}"
		
		unset 'options[DEFAULT]'
	fi
	
	# Give feedback when encountering unexpected options
	local extra_opts="$(
		if [ -n "${BASH_VERSION:-}" ];
		then
			eval 'echo "${!options[@]}"'  # This needs to be eval'd as it is not valid ZSH syntax
		else
			echo "${(k)options[@]}"  # Not valid BASH but not a syntax error unless the subsitution is actally performed
		fi
	)"
	if [[ ${extra_opts} ]];
	then
		_pam_env_log_error "${name}" "Unrecognized Option(s): ${extra_opts}"
		return 1
	fi
	
	return 0
}


function _pam_env_to_sh()
{
	local adv_name=""
	local adv_cont=""
	
	while read -r line;
	do
		if [ -n "${adv_name}" ];  # Continuation line
		then
			adv_cont+="${line}"
			if [[ ${adv_cont:(-1)} = '\' ]];
			then
				adv_cont="${adv_cont:0:(-1)}"
				continue  # Keep accumulating contents from further continuation lines
			fi
		else
			line="$(_pam_env_trim "${line}")"
			if [ -z "${line}" ] || [ "${line:0:1}" = "#" ];
			then
				continue;
			fi
			
			# Advanced mode
			if [[ ${line} = *['	 ']* ]];
			then
				# Split up line into variable name and remainder at the first
				# whitespace block
				adv_cont="${line#*[[:blank:]]}"
				adv_name="${line:0:$((${#line}-${#adv_cont}-1))}"
				adv_cont="$(_pam_env_ltrim "${adv_cont}")"
				if [[ ${adv_cont:(-1)} = '\' ]];
				then
					adv_cont="${adv_cont:0:(-1)}"
					continue  # Accumulate contents from continuation lines
				fi
			
			# Classic assignment mode
			elif [[ ${line} = *=* ]];
			then
				# Escape everything using single quotes to ensure that no
				# substitution will be performed by the shell
				line="$(_escape "${line}")"
				
				# Add eval command with export
				printf "export %s\n" "${line}"
			
			# Variable clearing mode
			else
				# Escape everything using single quotes to ensure that no
				# substitution will be performed by the shell
				line="$(_escape "${line}")"
				
				# Add eval command with unset
				printf "unset %s\n" "${line}"
			fi
		fi
		
		if [ -n "${adv_name}" ];  # Finished reading contents of advanced assignment
		then
			_pam_env_parse_adv "${adv_name}" "${adv_cont}" ||:
			adv_name=""
			adv_cont=""
		fi
	done
}

function _escape()
{
	line="$1"
	printf "%s" "'${line//'/'\"'\"'}'"
}

if [ -r ~/.pam_environment ];
then
	eval "$(_pam_env_to_sh < ~/.pam_environment)"
fi
