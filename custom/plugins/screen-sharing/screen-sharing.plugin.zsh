if [[ -z ${STY} ]];
then
	# Running in screen

	screen -q -list ${USER}.zsh
	if [ $? -lt 10 ];
	then
		# No screen session running

		# Start new screen session in HOME directory
		exec screen ${=SCREENFLAGS}    -S ${USER}.zsh
	else
		# Attach to existing screen session
		exec screen ${=SCREENFLAGS} -x -S ${USER}.zsh
	fi
fi

unset SCREENFLAGS