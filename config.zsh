# Use shipped less keyboard configuration
export LESSKEY=${ZSH_CONFIG_HOME}/config/less/keys

# Use shipped nano user-configuration if there is no conflict with the real user config
if [[ ! -f ~/.nanorc && ! -f ${XDG_CONFIG_HOME:-~/.config}/nano/nanorc ]];
then
	alias nano="env XDG_CONFIG_HOME=\${ZSH_CONFIG_HOME}/config nano"
fi

# Change or reset tab size
if type tabs >/dev/null 2>&1;
then
	if [ -z "${TABSIZE:-}" ];
	then
		tabs -8
	else
		tabs "${TABSIZE}"
	fi
fi

# Enable advanced lesspipe (check for all required non-coreutils or non-POSIX commands)
if { type file && type egrep && type tput; } >/dev/null 2>&1;
then
	export LESSOPEN="|lesspipe.sh %s"
fi
